# Title 

Unable to capture view hierarchy

# Basic information

| Key | Value |
|---|---|
| # | FB8111494 |
| Status | Closed |
| Area | Xcode |
| Type of issue | Incorrect/Unexpected Behavior |
| Date | 2020-07-23 |
| Xcode | 12.0 beta 2 (12A6163b) |

# Description

While attempting to create an isolated project for another SwiftUI bug, running the compiled code in Simulator ends up with a blank screen and the following warning message;

```
Error:    Unable to Capture View Hierarchy.
Details:  Log Title: Data source expression execution failure.
Log Details: error evaluating expression “(id)[[(Class)objc_getClass("DBGTargetHub") sharedHub] performRequestWithRequestInBase64:@"…"]”: error: Execution was interrupted, reason: EXC_BAD_ACCESS (code=1, address=0x0).
The process has been returned to the state before expression evaluation.

Log Method: -[DBGDataSourceConnectionLibViewDebugger _executeLLDBExpression:forRequest:onPotentialThread:iteration:]_block_invoke
Method:   -[DBGViewDebugger _initiateInitialRequestWithDataSourceVersion:]_block_invoke
Environment: Xcode 12.0 (12A6163b) debugging iPhone SE (2nd generation) iOS Simulator 14.0 (18A5319g).
Please file a bug at https://feedbackassistant.apple.com with this warning message and any useful information you can provide.
```

Please check the attached project.

The Canvas Preview for the SingleSelectionToggleView also turns up white, presumably for the same reason.

# Steps

1. Open attached project.
2. View Canvas Preview for SingleSelectionToggleView.swift.
3. Run project in iOS simulator.


## Expected

I expect to see three buttons, one of which has a background.

## Actual

I only see a blank/white screen and cannot capture the view hierarchy for UI debugging.

# Resolution

Seems fixed in Xcode 12.0 beta 3.
