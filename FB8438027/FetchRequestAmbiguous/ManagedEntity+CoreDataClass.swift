//
//  ManagedEntity+CoreDataClass.swift
//  FetchRequestAmbiguous
//
//  Created by Bruno Scheele on 17/08/2020.
//  Copyright © 2020 Bruno Scheele. All rights reserved.
//
//

import Foundation
import CoreData

@objc(ManagedEntity)
public class ManagedEntity: NSManagedObject {

}
