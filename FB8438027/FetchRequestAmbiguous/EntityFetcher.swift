//
//  EntityFetcher.swift
//  FetchRequestAmbiguous
//
//  Created by Bruno Scheele on 14/08/2020.
//  Copyright © 2020 Bruno Scheele. All rights reserved.
//

import Foundation

class EntityFetcher {
    func fetch() {
        let request = ManagedEntity.fetchRequest()
    }
}
