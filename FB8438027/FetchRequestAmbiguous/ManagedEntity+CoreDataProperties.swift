//
//  ManagedEntity+CoreDataProperties.swift
//  FetchRequestAmbiguous
//
//  Created by Bruno Scheele on 17/08/2020.
//  Copyright © 2020 Bruno Scheele. All rights reserved.
//
//

import Foundation
import CoreData


extension ManagedEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ManagedEntity> {
        return NSFetchRequest<ManagedEntity>(entityName: "Entity")
    }


}

extension ManagedEntity : Identifiable {

}
