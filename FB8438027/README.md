# Title

Managed Object's generated `fetchRequest` function results in 'ambiguous code

# Basic information

| Key | Value |
|---|---|
| # | FB8438027 |
| Status | Closed |
| Area | Xcode |
| Type of issue | Incorrect/Unexpected Behavior |
| Date | 2020-08-17 |
| Xcode | 12.0 beta 4 (12A8179i) |

# Description

When generating code for a Core Data model, one of the generated functions is a convenience method for creating a NSFetchRequest for the given object. For example:

```
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Entity> {
        return NSFetchRequest<Entity>(entityName: "Entity")
    }
```

However, using this function results in a compiler error;

```
Ambiguous use of 'fetchRequest()’
```

This class function should be usable as is.

See also attached screenshot.

# Steps

1. Open the attached project.
2. Open file `EntityFetcher.swift`.
3. Build the project.

(See attached video ‘Reproduction’)

## Expected

The project builds without errors.

## Actual

Building the project fails with a compiler error.

# Resolution

Outdated. Project doesn't build anymore for unrelated reasons.
