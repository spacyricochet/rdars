# Title

Closing a simulator causes appleaccountd crash

# Basic information

| Key | Value |
|---|---|
| # | FB9499238 |
| Status | Closed |
| Area | Xcode |
| Type of issue | Application Crash |
| Xcode | 13.0 beta 4 (13A5201i) |

# Description

After running a project in a simulator in Xcode, closing the simulator (whether through ⌘+W or ⌘+Q) causes appleaccountd to become unresponsive and/or crash.

# Steps

1. Open Xcode.
2. Create a new SwiftUI iOS app project (or, run attached project).
3. Run project in a simulator.
4. Close the simulator.


## Expected

No appleaccountd crash.

## Actual

appleaccountd crashes.

# Resolution

Has been resolved.
