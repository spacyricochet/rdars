//
//  AppleAccountDCrashApp.swift
//  AppleAccountDCrash
//
//  Created by Bruno Scheele on 13/08/2021.
//

import SwiftUI

@main
struct AppleAccountDCrashApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
