# Title

Moving a non-folder group from one folder-group to another, moves the entire folder

# Basic information

| Key | Value |
|---|---|
| # | FB6341401 |
| Status | Close |
| rdar | 33744915 | 
| Area | iOS & iPadOS | SwiftUI Framework |
| Type of issue | Incorrect/Unexpected Behavior |
| Xcode | 9.0 beta 4 (9M189t) |

# Description

Moving a non-folder group from one folder-group to another, moves the entire folder
Which product are you seeing an issue with?
iOS + SDK
Which area are you seeing an issue with?
Description
Please describe the issue and what steps we can take to reproduce it
Summary:
When moving a non-folder group from one folder-group A to another folder-group B, Xcode moves the entire originating folder A to folder B.

This breaks the build on the project, because the original folder and file references in the project are lost.

## Steps

1. Start new project.
2. Create a group ‘SomeFiles’ with a Folder.
3. Add a file in the group ‘SomeFiles’.
4. Create a group ‘SomeOtherFiles’ with a Folder.
5. Create a file in the group ‘SomeOtherFiles’.
6. Prepare some data files in the project root folder. Don’t add them to the project yet.
7. Create a ‘Group without a Folder’ named ‘Data’ in the group ‘SomeFiles’.
8. Add the data files to the group ‘Data’ without copying them. Uncheck ‘Copy items if needed’.
9. Move the folder ‘Data’ from the group ‘SomeFiles’ to the group ‘SomeOtherFiles’.


## Expected

*The group ‘Data’ with its contents gets moved to the group ‘SomeOtherFiles’.
* No other changes are made to the project and folder structure.

## Actual

* ✅ The group ‘Data’ with its contents gets moved to the group ‘SomeOtherFiles’.
* ❌ The entire folder ‘SomeFiles’ is moved to the folder ‘SomeOtherFiles’
* ❌ Because the folder ‘SomeFiles’ along with the file ‘SomeFile’ has moved, the project loses the reference. The project wouldn’t compile anymore.

# Files

Added a .zip with an example Xcode project. It includes the git repository with a 'before' and 'after' commit and two screenshots illustrating the folder structure 'before' and 'after'.

![Before]("1.AfterPreparation.png")

![After]("2.AfterMovingData.png")

# Resolution

Unknown. Access to original ticket lost.
