# Title 

Toggle should allow customizing 'off' state background color

# Basic information

| Key | Value |
|---|---|
| # | FB13819280 |
| Status | Open |
| Topic | Developer Technologies & SDK's |
| Area | iOS |
| Technology | SwiftUI |
| Type of issue | Suggestion |
| Date | yyyy-MM-dd |
| Xcode | version |
| OS | version |

# Description

It would be great if we could customize the Toggle, specifically the off-state background color.

Currently, the toggle’s ‘off’ state’s background color doesn’t meet the 3:1 contrast ratio, failing it during accessibility audits. The ‘Increase contrast’ mode doesn’t allow it to be passed, which is why we need to customize the control to pass the WCAG 2.2 1.4.3 Contrast (minimum) AA criteria.

However, toggle doesn’t allow the developer to customize the ‘off’ state background setting, which is why we need workaround instead. It would be great if we could get access to more Toggle parts to customize beyond the tint color.

# Steps

1. Open attached project
2. Run app, or preview ContentView in Xcode
3. See code for suggestions

## Expected

* A better way of adjusting the Toggle ‘off’ state background, without having to rebuild the entire component with the current ToggleStyle

## Actual

* Hacks are needed to adjust the Toggle background

# Files

* Example project; ToggleLab

# Known duplicates

* FB13816580
