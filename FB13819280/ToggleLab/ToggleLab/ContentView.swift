import SwiftUI

struct ContentView: View {
	
	@State var isOn: Bool = false
	
	var body: some View {
		VStack(alignment: .leading) {
			Text("Changing the Toggle background")
				.font(.headline)
				.accessibilityAddTraits(.isHeader)
			Text("Currently done with a Capsule background")
				.font(.subheadline)
			Divider()
			Toggle(isOn: $isOn, label: { Text("Toggle in off state") })
				.labelsHidden()
				.padding(.trailing, 2)
				.background(.gray, in: .capsule)
			  // What we would like instead of the above;
			  //.toggleStyle(.color(background: .gray))
			Divider()
			Text(
	 """
	 It would be great if we had an easy modifier to change the 'off' state background color. Or, if we could use a custom ToggleStyle to update just the appearance of a toggle instead of having to create an entirely new Toggle view.")
	 
	 This can be worked around with a Capsule background, though that doesn't fit the Toggle background _exactly_. And requires the `labelsHidden()` modifier, which isn't always preferred.
	 """
			)
		}
		.padding()
	}
}

#Preview {
	ContentView()
}
