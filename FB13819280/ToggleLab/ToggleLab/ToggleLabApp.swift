import SwiftUI

@main
struct ToggleLabApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
