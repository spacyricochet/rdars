//
//  ContentView.swift
//  MultiLineLabel
//
//  Created by Bruno Scheele on 28/06/2019.
//  Copyright © 2019 Binary Adventures. All rights reserved.
//

import SwiftUI

struct ContentView : View {
    var body: some View {
        VStack {
            Text("Multi-line example")
                .font(.largeTitle)
                .padding(.bottom, 12)
            GeometryReader { geometry in
                ScrollView {
                    Text("""
This is a line of text that realy has no place fitting on a phone screen.

And in case we come up with the idea to use a tablet screen, we add a couple of extra paragraphs, forcing us to show multiple lines.

Preferably more than four, since that's where it went wrong in my use case.
"""
                        )
                        .font(.body)
                        .lineLimit(nil)
                        .frame(width: geometry.size.width)
                }
            }
            
            Text("If you don't wrap the text view in a scrollview, line limit isn't respected either.")
            .lineLimit(nil)
            
            Button(action: sendFeedbackTapped) {
                Text("Send feedback")
            }
            Button(action: changeColorTapped) {
                Text("Change colors")
            }
        }
    }
    
    func sendFeedbackTapped() {
        // Placeholder
    }
    
    func changeColorTapped() {
        // Placeholder
    }
}

#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
#endif
