# [CLOSED] Linelimit >1 in Text View not respected

## Basic information

| Key | Value |
|---|---|
| # | FB6341401 |
| Status | Closed |
| Area | iOS & iPadOS | SwiftUI Framework |
| Type of issue | Incorrect/Unexpected Behavior |
| Xcode | 11.0 beta 2 (11M337n) |
| OS | iOS 13.0 beta 2 |

## Description

It is currently unintuitive and harder than it could be to show a Text View with multiple lines.

* In the regular case, the when setting `Text(shakespearOpus).lineLimit(nil)`, the amount of text shown is only one line, truncated at the end. 
* In a workaround, the Text View is wrapped in a ScrollView. This does display multiple lines. However, the text still does not show completely and is instead cut off after a seemingly random amount of lines.

See the attached Example project.

## Files

![Screenshot](Screenshot.png)

## Resolution

Unknown. Access to original ticket lost.