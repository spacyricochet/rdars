# Title 

Increase contrast toggle in Settings should preview its effect

# Basic information

| Key | Value |
|---|---|
| # | FB13819260 |
| Status | Open |
| Topic | iOS & iPadOS |
| Area | Settings > Accessibility|
| Type of issue | Suggestion |
| Date | 2024-06-06 |
| OS | iOS 17.5.1 (21F90) |

# Description

The button to enable ‘Increase contrast’ in ‘Settings > Accessibility > Display & Text Size’ should be high contrast when the mode is off.

An accessibility audit to receive a WCAG 2.2 AA certification for our app fails the native iOS toggle on the WCAG 1.4.3 Contrast (minimum) AA criteria, because the background of the ‘off’ state doesn’t have a high enough contrast with the background. 
We noted that it should still be passed, since it’s a native iOS control and there is a setting to increase the contrast on the entire device, which would affect this. However, the auditors noted that the existence of ‘Increase contrast’ doesn’t help, because the toggle to enable the setting _also_ fails the same criteria.

If the button would previews the high contrast setting, just like the toggle for ‘On/Off labels’ already has the labels visible, this would appease the auditors for all apps.

## Steps

1. Open Settings
2. Go to ‘Accessibility’
3. Go to ‘Display & Text Sizes’
4. Turn ‘Increase contrast’ off
5. Check toggle appearance

## Expected

* The toggle looks as if ‘Increase contrast’ is already on

## Actual

* The toggle looks like all the other toggles in the ‘off’ state

# Files

* ![Current - Light mode](Current\ -\ Light\ mode.jpeg)
* ![Current - Dark mode](Current\ -\ Dark\ mode.jpeg)
* ![Expected - Light mode](Expected\ -\ Dark\ mode.jpeg)
* ![Expected - Dark mode](Expected\ -\ Dark\ mode.jpeg)

# Known duplicates

* FB13816553
