# Title

iOS Toggle background in 'off' state fails WCAG 2.2 Contrast (minimum) AA criterium

# Basic information

| Key | Value |
|---|---|
| # | FB13819105 |
| Status | Open |
| Topic | Developer Technologies and SDK’s |
| Area | iOS |
| Technology | SwiftUI |
| Type of issue | Incorrect/Unexpected Behavior |
| Date | 2024-06-06 |
| Xcode | Version 15.4 (15F31D) |
| OS | iOS 17.5 Seed 4 (21F5073b) |

# Description

The contrast of the toggle background (1.21:1) doesn't meet the 3:1 contrast threshold to pass the criterium. Audits are currently failing this, as the high contrast setting for iOS fails on the same issue.

## Steps

1. Open attached project
2. Run, or preview ContentView
3. Look at toggle background color, contrasted to the screen background
4. Toggle Dark Mode
5. Look at toggle background color, contrasted to the screen background

## Expected

* Contrast of toggle against white background meets 3:1 contrast threshold
* Contrast of toggle against black background meets 3:1 contrast threshold

## Actual

* Contrast of toggle against white background is 1.21:1
* Contrast of toggle against black background is 1.29:1

# Files

* [A toggle's background color is R233, G233, B235 in light mode](Digital\ Colour\ Meter\ -\ Light\ mode.png)
* Example project; ToggleLab

# Known duplicates

* FB13816538
