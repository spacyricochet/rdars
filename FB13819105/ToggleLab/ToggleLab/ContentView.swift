import SwiftUI

struct ContentView: View {
		var body: some View {
			VStack(alignment: .leading) {
				Text("Contrast (minimum)")
					.font(.headline)
					.accessibilityAddTraits(.isHeader)
				Text("WCAG SC 1.4.3 (Level AA)")
					.font(.subheadline)
				Divider()
				Toggle(isOn: .constant(false), label: { Text("Toggle in off state") })
				Divider()
				Text("The contrast of the toggle background (1.21:1) doesn't meet the 3:1 contrast threshold to pass the criterium. Audits are currently failing this, as the high contrast setting for iOS fails on the same issue.")
				Divider()
				Button {
					UIApplication.shared.open(URL(string: "https://webaim.org/resources/contrastchecker/?fcolor=E9E9EB&bcolor=FFFFFF")!) } label: {
						Text("WebAIM contrast checker")
					}
				Button {
					UIApplication.shared.open(URL(string: "https://www.w3.org/WAI/WCAG22/Understanding/contrast-minimum.html")!) } label: {
						Text("WCAG: Understanding 1.4.3")
					}
			}
			.padding()
		}
}

#Preview {
		ContentView()
}
