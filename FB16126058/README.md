# Title 

Activating a focused area in a Swift Chart with Full Keyboard Access does not activate chartXSelection

# Basic information

| Key | Value |
|---|---|
| # | FB16126058 |
| Status | Open |
| Platform | iOS |
| Technology | SwiftUI |
| Type of issue | Incorrect/Unexpected Behavior |
| Date | 2024-12-19 |
| Xcode | Version 16.0 (16A242d) |
| OS | iOS 18.1.1 (22B91) |

# Description

A Swift Chart is very accessible out of the box, but Full Keyboard Access appears to be lacking. In a chart that uses `chartXSelection` to bind the currently activated data point, it is expected that focusing on a chart area with Full Keyboard Access and tapping spacebar to activate, would similarly activate and bind the (nearest) data point in that area.

However, while all areas are traversable with tab, pressing spacebar instead does not do anything. This feels like an oversight in the supposed ‘free accessibility’ that Swift Chart promises. Notably, many of the charts in Apple’s apps (such as Health) also have very inconsistent support for Full Keyboard Access.

Because Full Keyboard Access support on the charts is necessary to pass our WCAG 2.2 AA audit, the lack of proper support on Swift Charts would force our organization to add a custom data sheet with all the data in a table as an alternative, on every chart in our banking app.

# Steps

1. Open attached project
2. Run project on device and wait until it launches
3. Enable Full Keyboard Access on device
4. Press tab on keyboard to start focusing
5. Keep pressing tab to highlight an area in the graph
6. Press spacebar

## Expected

* While pressing spacebar, the datapoint is marked as selected by `chartXSelection` and an annotation with the data points values is shown on the graph.

## Actual

* Nothing happens

# Files

* [ChartsFKSSupport](./ChartsFKSSupport/)
