import SwiftUI

struct ContentView: View {
    var body: some View {
			NavigationView {
				VStack(alignment: .leading) {
					Text("When using Full Keyboard Support, the expectation is that you can focus on and activate the same elements that are exposed to VoiceOver and Voice Control.")
					
					ChartFKSLab()
					
					Text("So far, it looks like the elements are exposed, but activating them doesn't activate with chartXSelection.")
				}
				.scenePadding()
				.navigationTitle("Chart FKS Support")
			}
    }
}

#Preview {
    ContentView()
}
