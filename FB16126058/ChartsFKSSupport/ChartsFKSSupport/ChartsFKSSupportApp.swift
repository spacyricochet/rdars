import SwiftUI

@main
struct ChartsFKSSupportApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
