import SwiftUI
import Charts

@available(iOS 17.0, *)
struct ChartFKSLab: View {
	@State var selectedDate: Date?

	var body: some View {
		Chart(dataPoints) { dataPoint in
			LineMark(x: .value("time", dataPoint.date), y: .value("percentage", dataPoint.percentage))
				.foregroundStyle(Color.purple)

			if let selectedDate, dataPoint.sameDate(as: selectedDate) {
				PointMark(x: .value("time", dataPoint.date), y: .value("percentage", dataPoint.percentage))
					.foregroundStyle(Color.blue)
					.annotation {
						VStack {
							Text(dataPoint.dateString)
								.font(.body)
								.foregroundStyle(Color.primary)
							Text(dataPoint.percentage.formatted(.percent))
								.font(.body)
								.foregroundStyle(Color.primary)
						}
						.padding(.all, 8)
							.background(Color.gray)
							.cornerRadius(4)
					}
			}
		}
		.chartXSelection(value: $selectedDate)
	}
}

private struct LineData: Identifiable, Equatable, Hashable {
	var id: Int
	let date: Date
	let percentage: Double

	var dateString: String {
		date.formatted(date: .numeric, time: .omitted)
	}
}

private extension LineData {
	func sameDate(as comparison: Date) -> Bool {
		self.date.formatted(date: .numeric, time: .omitted) == comparison.formatted(date: .numeric, time: .omitted)
	}
}

private let dataPoints: [LineData] = [
	LineData(id: 0, date: makeDate(year: 2022, month: 10, day: 5), percentage: 10),
	LineData(id: 1, date: makeDate(year: 2022, month: 10, day: 6), percentage: 20),
	LineData(id: 2, date: makeDate(year: 2022, month: 10, day: 7), percentage: 10),
	LineData(id: 3, date: makeDate(year: 2022, month: 10, day: 8), percentage: 50),
	LineData(id: 4, date: makeDate(year: 2022, month: 10, day: 9), percentage: 70),
	LineData(id: 5, date: makeDate(year: 2022, month: 10, day: 10), percentage: 40),
	LineData(id: 6, date: makeDate(year: 2022, month: 10, day: 11), percentage: 60),
	LineData(id: 7, date: makeDate(year: 2022, month: 10, day: 12), percentage: 50)
]

private func makeDate(year: Int, month: Int, day: Int) -> Date {
	let components = DateComponents(year: year, month: month, day: day)
	return Calendar.current.date(from: components)!
}
