# Title

Show selected variants in Canvas instead of all

## Basic information

| Key | Value |
|---|---|
| # | FB13232172 |
| Status | Open |
| Area | Xcode |
| Type of issue | Suggestion |
| Date | 2023-10-04 |
| Xcode | Version 15.0 (15A240d) |

# Description

I would like to be able to select which variants are shown in the Preview Canvas, instead of only showing all possible variants for the selected setting.

# Steps

While developing with SwiftUI, it’s helpful to preview specific different device settings alongside each other in the Canvas. While the current ‘variants’ buttons allows for this, it only provides an all or nothing approach. This is especially visible in the Dynamic Type Variants option, where you are shown twelve different previews, for all possible font size selections.

Especially here, it is often more helpful to only show the default font, along with one or two other sizes. For example, I would prefer it to show the smallest, default and largest font size possible, as illustrated in the attached mockup.

It would be extremely helpful if you can select which variants will be shown of the selected device setting, instead of showing all of them.
