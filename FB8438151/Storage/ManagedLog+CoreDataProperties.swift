//
//  ManagedLog+CoreDataProperties.swift
//  Storage
//
//  Created by Bruno Scheele on 17/08/2020.
//
//

import Foundation
import CoreData


extension ManagedLog {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ManagedLog> {
        return NSFetchRequest<ManagedLog>(entityName: "Log")
    }

    @NSManaged public var id: UUID?
    @NSManaged public var entries: NSSet?
    @NSManaged public var child: ManagedChild?

}

// MARK: Generated accessors for entries
extension ManagedLog {

    @objc(addEntriesObject:)
    @NSManaged public func addToEntries(_ value: ManagedEntry)

    @objc(removeEntriesObject:)
    @NSManaged public func removeFromEntries(_ value: ManagedEntry)

    @objc(addEntries:)
    @NSManaged public func addToEntries(_ values: NSSet)

    @objc(removeEntries:)
    @NSManaged public func removeFromEntries(_ values: NSSet)

}

extension ManagedLog : Identifiable {

}
