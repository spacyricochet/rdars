import CoreData
import Models
#if os(iOS)
import UIKit
#endif

enum StorageType {
    case persistent
    case inMemory
}

class DataStorage: ObservableObject {
    
    let persistentContainer: NSPersistentContainer
    
    public init(_ storeType: StorageType, notificationCenter: NotificationCenter = .default) throws {

        #warning("Uncomment the following code as a workaround.")
//        let momdName = "DataStorage"
//        guard
//            let modelUrl = Bundle(for: type(of: self)).url(forResource: momdName, withExtension:"momd"),
//            let managedObjectModel = NSManagedObjectModel(contentsOf: modelUrl) else {
//            preconditionFailure("Error loading model named '\(momdName)' from bundle.")
//        }
//        self.persistentContainer = NSPersistentContainer(name: momdName, managedObjectModel: managedObjectModel)
        
        #warning("Expected the following line to work. However, the tests fail.")
        self.persistentContainer = NSPersistentContainer(name: "DataStorage")
        
        if storeType == .inMemory {
            let description = NSPersistentStoreDescription()
            description.url = URL(fileURLWithPath: "/dev/null")
            persistentContainer.persistentStoreDescriptions = [description]
        }
        
        persistentContainer.loadPersistentStores { (description, error) in
            guard error == nil else {
                preconditionFailure("Could not load persistent store: \(error?.localizedDescription ?? "<no error supplied>")")
            }
        }
        
        setupSaveContextOnResignActive(notificationCenter)
    }
}

private extension DataStorage {
    func setupSaveContextOnResignActive(_ center: NotificationCenter) {
        #if os(iOS)
        center.addObserver(forName: UIApplication.willResignActiveNotification,
                           object: nil,
                           queue: nil) { [weak self] (notification) in
            self?.persistentContainer.viewContext.saveOrRollBack()
        }
        #endif
    }
}
