import XCTest
import CoreData
@testable import Storage

class Storage_ObjectsTests: XCTestCase {

    var storage: DataStorage!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        storage = try DataStorage(.inMemory)
    }

    override func tearDownWithError() throws {
        storage = nil
        try super.tearDownWithError()
    }

    func test_createLog_createsAnEmptyLog() throws {
        storage.createLog()
        
        let request = NSFetchRequest<ManagedLog>(entityName: "Log")
        
        let fetched = try storage.persistentContainer.viewContext.fetch(request)
        
        XCTAssertEqual(fetched.count, 1)
        XCTAssertEqual(fetched.first?.child?.id, fetched.first?.id)
        XCTAssertTrue(fetched.first?.entries?.allObjects.isEmpty == true)
    }
}
