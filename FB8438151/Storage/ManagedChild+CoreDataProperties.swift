//
//  ManagedChild+CoreDataProperties.swift
//  Storage
//
//  Created by Bruno Scheele on 17/08/2020.
//
//

import Foundation
import CoreData


extension ManagedChild {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ManagedChild> {
        return NSFetchRequest<ManagedChild>(entityName: "Child")
    }

    @NSManaged public var birthday: Date?
    @NSManaged public var dueDate: Date?
    @NSManaged public var familyName: String?
    @NSManaged public var gender: String?
    @NSManaged public var givenName: String?
    @NSManaged public var id: UUID?
    @NSManaged public var log: ManagedLog?

}

extension ManagedChild : Identifiable {

}
