//
//  ManagedEntry+CoreDataProperties.swift
//  Storage
//
//  Created by Bruno Scheele on 17/08/2020.
//
//

import Foundation
import CoreData


extension ManagedEntry {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ManagedEntry> {
        return NSFetchRequest<ManagedEntry>(entityName: "Entry")
    }

    @NSManaged public var amountUnit: String?
    @NSManaged public var amountValue: Double
    @NSManaged public var category: String?
    @NSManaged public var customLabel: String?
    @NSManaged public var endDate: Date?
    @NSManaged public var id: UUID?
    @NSManaged public var note: String?
    @NSManaged public var startDate: Date?
    @NSManaged public var subcategory: String?
    @NSManaged public var log: ManagedLog?

}

extension ManagedEntry : Identifiable {

}
