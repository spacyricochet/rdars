import CoreData

extension NSManagedObjectContext {
    func saveOrRollBack() {
        if hasChanges {
            do {
                try save()
            } catch {
                print("⚠️ Could not save context: \(error)")
                rollback()
            }
        }
    }
}
