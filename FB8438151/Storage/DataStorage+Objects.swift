import CoreData
import Models

// MARK: - Log
extension DataStorage {
    
    /// Creates a new initial log, with an empty child and an empty list of entries.
    /// - Note: The child will use the same id as the log itself.
    func createLog() {
        let id = UUID()
        let log = ManagedLog(context: persistentContainer.viewContext)
        log.id = id
        
        let child = ManagedChild(context: persistentContainer.viewContext)
        child.id = id
        log.child = child
        
        log.entries = []
    }
    
    /// Fetches all availables logs.
    func logs() -> [ManagedLog] {
        do {
            let request = NSFetchRequest<ManagedLog>(entityName: "Log")
            return try persistentContainer.viewContext.fetch(request)
        } catch {
            print("⚠️ Error while fetching Logs; \(error)")
            return []
        }
    }
    
    /// Fetches the log with the specified id.
    func fetchLog(id: UUID) -> ManagedLog? {
        return logs().first { $0.id == id }
    }
}

// MARK: - Child
extension DataStorage {
    
    /// Convenience method for fetching the child with the specified id.
    public func fetchChild(id: UUID) -> ManagedChild? {
        return fetchLog(id: id)?.child
    }
    
    /// Saves the child in the persistant store, using create or update as appropriate.
    public func saveEntry(from model: Child) {
        if model.id == nil {
            createChild(from: model)
        } else {
            updateChild(from: model)
        }
    }
    
    /// Creates a child object from the given model.
    func createChild(from model: Child) {
        let childObject = ManagedChild(context: persistentContainer.viewContext)
        childObject.id = UUID()
        apply(model, to: childObject)
    }
    
    /// Updates child from the given model.
    func updateChild(from model: Child) {
        guard let modelId = model.id else {
            print("⚠️ Attempting to update object from unsaved model.")
            return
        }
        let fetchRequest = NSFetchRequest<ManagedChild>(entityName: "Child")
        fetchRequest.predicate = NSPredicate(format: "id == %@", modelId as CVarArg)
        
        guard let children = try? persistentContainer.viewContext.fetch(fetchRequest),
              children.count == 1,
              let child = children.first else {
            print("⚠️ Attempted to update child that is not present in context; \(model)")
            return
        }
        
        apply(model, to: child)
    }
    
    private func apply(_ model: Child, to object: ManagedChild) {
        object.givenName = model.givenName
        object.familyName = model.familyName
        object.dueDate = model.dueDate
        object.birthday = model.birthday
        object.gender = model.gender.rawValue
    }
}

// MARK: - Entry
extension DataStorage {
    
    /// Returns the entries of the child with the given id.
    public func fetchEntries(ofChildWithId id: UUID) -> Set<ManagedEntry> {
        if let result = fetchLog(id: id)?.entries as? Set<ManagedEntry> {
            return result
        } else {
            print("⚠️ Attempting to fetch entries of unsaved child.")
            return []
        }
    }
    
    /// Returns a fetch request for the entries of the child with the given id.
    public func fetchRequestForEntries(ofChildWithId id: UUID, sortDescriptors: [NSSortDescriptor]) -> NSFetchRequest<ManagedEntry> {
        let result = NSFetchRequest<ManagedEntry>(entityName: "Entry")
        result.predicate = NSPredicate(format: "child.id == %@", id as CVarArg)
        result.sortDescriptors = sortDescriptors
        return result
    }
    
    /// Saves the entry in the persistant store, using create or update as appropriate.
    public func saveEntry(from model: Entry) {
        if model.id == nil {
            createEntry(from: model)
        } else {
            updateEntry(from: model)
        }
    }
    
    /// Creates a entry object from the given model.
    func createEntry(from model: Entry) {
        let entryObject = ManagedEntry(context: persistentContainer.viewContext)
        entryObject.id = UUID()
        apply(model, to: entryObject)
    }
    
    /// Updates entry from the given model.
    func updateEntry(from model: Entry) {
        guard let modelId = model.id else {
            print("⚠️ Attempting to update object from unsaved model.")
            return
        }
        let fetchRequest = NSFetchRequest<ManagedEntry>(entityName: "Entry")
        fetchRequest.predicate = NSPredicate(format: "id == %@", modelId as CVarArg)
        
        guard let entries = try? persistentContainer.viewContext.fetch(fetchRequest),
              entries.count == 1,
              let entry = entries.first else {
            print("⚠️ Attempted to update entry that is not present in context; \(model)")
            return
        }
        
        apply(model, to: entry)
    }
    
    private func apply(_ model: Entry, to object: ManagedEntry) {
        object.category = model.category
        object.subcategory = model.subcategory
        object.customLabel = model.customLabel
        object.startDate = model.startDate
        object.endDate = model.endDate
        object.amountValue = model.amount?.value ?? 0
        object.amountUnit = model.amount?.unit
    }
}
