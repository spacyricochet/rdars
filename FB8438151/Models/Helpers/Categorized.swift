protocol Category {
	var category: String { get }
	var subcategory: String? { get }
	var customLabel: String? { get }
}

protocol Subcategory {
	var subcategory: String? { get }
	var customLabel: String? { get }
}
