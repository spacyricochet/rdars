import Foundation

extension UUID {
    /// Creates a UUID from the given `optionalString`. Returns nil if the string is `nil`.
    /// If the string can not be decoded into a UUID, throws the given `error`.
    init?(optionalString: String?, errorToThrowOnInvalidId error: Error) throws {
        guard let string = optionalString else {
            return nil
        }
        guard let result = UUID(uuidString: string) else {
            throw error
        }
        self = result
    }
}
