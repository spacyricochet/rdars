import Foundation

public struct Entry: Identifiable {
	public let id: UUID?
	public var category: String
	public var subcategory: String?
	public var customLabel: String?
	public var startDate: Date
	public var endDate: Date
	public var amount: Amount?
    public var note: String?
	
	public init(id: UUID?, category: String, subcategory: String?, customLabel: String?, startDate: Date, endDate: Date, amount: Amount?) {
		self.id = id
		self.category = category
		self.subcategory = subcategory
		self.customLabel = customLabel
		self.startDate = startDate
		self.endDate = endDate
		self.category = category
		self.amount = amount
	}
	
	public init(id: UUID?, variant: EntryVariant, startDate: Date, endDate: Date, amount: Amount?) {
		self = Entry(id: id, category: variant.category, subcategory: variant.subcategory, customLabel: variant.customLabel, startDate: startDate, endDate: endDate, amount: amount)
	}
    
    public var variant: EntryVariant {
        do {
            let result = try EntryVariant(category: category,
                                          subcategory: subcategory,
                                          customLabel: customLabel)
			guard result.case != .unknown else {
				assertionFailure("Returning 'unknown' where should")
				return .unknown
			}
			return result
		} catch {
			assertionFailure("Caught error while initializing variant: \(error)")
			return .unknown
		}
	}
}

// MARK: - Hashable & Equatable
extension Entry: Hashable {
	public func hash(into hasher: inout Hasher) {
		hasher.combine(id)
	}
}
extension Entry: Equatable {}
