import Foundation

// MARK: - CodingKeys
extension Entry {
	enum CodingKeys: String, CodingKey {
		case id, variant
		case startDate = "start_date"
		case endDate = "end_date"
		case category, subcategory
		case customLabel = "custom_label"
	}
	
	enum AmountCodingKeys: String, CodingKey {
		case value = "amount_value"
		case unit = "amount_unit"
	}
}

// MARK: - Encodable
extension Entry: Encodable {
	public func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		
        try container.encodeIfPresent(id?.uuidString, forKey: .id)
		try container.encode(startDate, forKey: .startDate)
		try container.encode(endDate, forKey: .endDate)
		try container.encode(category, forKey: .category)
		try container.encodeIfPresent(subcategory, forKey: .subcategory)
		try container.encodeIfPresent(customLabel, forKey: .customLabel)
		
		if let amount = amount {
			var amountContainer = encoder.container(keyedBy: AmountCodingKeys.self)
			try amountContainer.encode(amount.value, forKey: .value)
			try amountContainer.encode(amount.unit, forKey: .unit)
		}
	}
}

// MARK: - Decodable
extension Entry: Decodable {
	
	enum DecodingError: Error {
		case invalidId
		case incompleteAmount
	}
	
	public init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		
        let id = try UUID(optionalString: container.decodeIfPresent(String.self, forKey: .id), errorToThrowOnInvalidId: DecodingError.invalidId)
		let startDate = try container.decode(Date.self, forKey: .startDate)
		let endDate = try container.decode(Date.self, forKey: .endDate)
		let category = try container.decode(String.self, forKey: .category)
		let subcategory = try container.decodeIfPresent(String.self, forKey: .subcategory)
		let customLabel = try container.decodeIfPresent(String.self, forKey: .customLabel)
		
		self.id = id
		self.startDate = startDate
		self.endDate = endDate
		self.category = category
		self.subcategory = subcategory
		self.customLabel = customLabel
		
		let amountContainer = try decoder.container(keyedBy: AmountCodingKeys.self)
		let amountValue = try amountContainer.decodeIfPresent(Double.self, forKey: .value)
		let amountUnit = try amountContainer.decodeIfPresent(String.self, forKey: .unit)
		guard (amountValue == nil && amountUnit == nil) || (amountValue != nil && amountUnit != nil) else {
			throw DecodingError.incompleteAmount
		}
		if let value = amountValue, let unit = amountUnit {
			self.amount = Amount(value: value, unit: unit)
		} else {
			self.amount = nil
		}
	}
}
