public enum BottleVariant {
	case formula
	case breastMilk
	case custom(String)
}

extension BottleVariant {
	enum Case: String {
		case formula
		case breastMilk
		case custom
	}
}

extension BottleVariant: RawRepresentable {
	
	public init?(rawValue: String) {
		switch rawValue {
			case "formula":
				self = .formula
			case "breastMilk":
				self = .breastMilk
			default:
				self = .custom(rawValue)
		}
	}
	
	public var rawValue: String {
		switch self {
			case .formula:
				return "formula"
			case .breastMilk:
				return "breastMilk"
			case .custom(let value):
				return value
		}
	}
}

extension BottleVariant: Subcategory {
	var subcategory: String? {
		switch self {
			case .formula, .breastMilk:
				return rawValue
			case .custom:
				return String.Constants.custom
		}
	}
	
	var customLabel: String? {
		guard case .custom(let value) = self else {
			return nil
		}
		return value
	}
}

// MARK: - Hashable & Equatable
extension BottleVariant: Hashable {}
extension BottleVariant: Equatable {}

// MARK: - Codable
extension BottleVariant: Decodable {}
extension BottleVariant: Encodable {}
