extension EntryVariant: Category {
	var category: String {
		`case`.rawValue
	}
	
	var subcategory: String? {
		switch self {
			case .unknown:
				return nil
			case .weight:
				return nil
			case .breast(let variant):
				return variant.rawValue
			case .bottle(let subcategory):
				return subcategory.subcategory
			case .solids(let subcategory):
				return subcategory.subcategory
			case .diaper(let subcategory):
				return subcategory.subcategory
			case .sleep(let subcategory):
				return subcategory.subcategory
			case .medicine(let subcategory):
				return subcategory.subcategory
		}
	}
	
	var customLabel: String? {
		switch self {
			case .unknown:
				return nil
			case .weight:
				return  nil
			case .breast:
				return  nil
			case .bottle(let subcategory):
				return  subcategory.customLabel
			case .solids(let subcategory):
				return  subcategory.customLabel
			case .diaper(let subcategory):
				return  subcategory.customLabel
			case .sleep(let subcategory):
				return  subcategory.customLabel
			case .medicine(let subcategory):
				return  subcategory.customLabel
		}
	}
}

// MARK: Decode and Encode from strings
extension EntryVariant {
	
	enum DecodingError: Error {
		case invalidCategory
		case invalidSubcategory
		case noCustomLabelWhereExpected
	}
	
	init(category: String, subcategory: String?, customLabel: String?) throws {
		guard let category = Case(rawValue: category) else {
			throw DecodingError.invalidCategory
		}
		switch category {
			case .unknown:
				self = .unknown
			case .breast:
				guard let subcategory = subcategory, let variant = BreastVariant(rawValue: customLabel ?? subcategory) else {
					throw DecodingError.invalidSubcategory
				}
				self = .breast(variant)
			case .weight:
				self = .weight
			case .bottle:
				guard let subcategory = subcategory, let variant = BottleVariant(rawValue: customLabel ?? subcategory) else {
					throw DecodingError.invalidSubcategory
				}
				self = .bottle(variant)
			case .solids:
				guard let subcategory = subcategory, let variant = SolidsVariant(rawValue: customLabel ?? subcategory) else {
					throw DecodingError.invalidSubcategory
				}
				self = .solids(variant)
			
			case .diaper:
				guard let subcategory = subcategory, let variant = DiaperVariant(rawValue: customLabel ?? subcategory) else {
					throw DecodingError.invalidSubcategory
				}
				self = .diaper(variant)
			
			case .sleep:
				guard let subcategory = subcategory, let variant = SleepVariant(rawValue: customLabel ?? subcategory) else {
					throw DecodingError.invalidSubcategory
				}
				self = .sleep(variant)
			
			case .medicine:
				guard let subcategory = subcategory, let variant = MedicineVariant(rawValue: customLabel ?? subcategory) else {
					throw DecodingError.invalidSubcategory
				}
				self = .medicine(variant)
		}
	}
}
