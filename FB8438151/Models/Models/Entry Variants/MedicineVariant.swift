public enum MedicineVariant {
	case shot
	case pill
	case vitamines
	case custom(String)
}

extension MedicineVariant: RawRepresentable {
	public init?(rawValue: String) {
		switch rawValue {
			case "shot":
				self = .shot
			case "pill":
				self = .pill
			case "vitamines":
				self = .vitamines
			default:
				self = .custom(rawValue)
		}
	}
	
	public var rawValue: String {
		switch self {
			case .shot:
				return "shot"
			case .pill:
				return "pill"
			case .vitamines:
				return "vitamines"
			case .custom(let value):
				return value
		}
	}
}

extension MedicineVariant: Subcategory {
	var subcategory: String? {
		return rawValue
	}
	
	var customLabel: String? {
		guard case .custom(let value) = self else {
			return nil
		}
		return value
	}
}

// MARK: - Hashable & Equatable
extension MedicineVariant: Hashable {}
extension MedicineVariant: Equatable {}

// MARK: - Codable
extension MedicineVariant: Decodable {}
extension MedicineVariant: Encodable {}

