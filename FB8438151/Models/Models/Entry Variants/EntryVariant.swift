public enum EntryVariant {
	case unknown
	case weight
	case breast(BreastVariant)
	case bottle(BottleVariant)
	case solids(SolidsVariant)
	case diaper(DiaperVariant)
	case sleep(SleepVariant)
	case medicine(MedicineVariant)
}

// MARK: - Cases
extension EntryVariant {
	public var `case`: Case {
		switch self {
			case .unknown: return .unknown
			case .weight: return .weight
			case .breast: return .breast
			case .bottle: return .bottle
			case .solids: return .solids
			case .diaper: return .diaper
			case .sleep: return .sleep
			case .medicine: return .medicine
		}
	}
	
	public enum Case: String, CaseIterable {
		case weight
		case breast
		case bottle
		case solids
		case diaper
		case sleep
		case medicine
		case unknown // Keep last, so it can be removed for `validCases`.
		
		public static var validCases: [Case] {
			return allCases.dropLast()
		}
	}
}

// MARK: - Hashable & Equatable
extension EntryVariant: Hashable {}
extension EntryVariant: Equatable {}
