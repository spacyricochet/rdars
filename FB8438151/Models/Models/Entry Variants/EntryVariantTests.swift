import XCTest
import Models

class EntryVariantTests: XCTestCase {

	func test_validCases_doesNotContainUnknown() {
		XCTAssertFalse(EntryVariant.Case.validCases.contains(.unknown))
	}

	func test_validCases_equalsAllCasesWithoutUnknown() {
		XCTAssertEqual(EntryVariant.Case.validCases, EntryVariant.Case.allCases.dropLast())
	}
}
