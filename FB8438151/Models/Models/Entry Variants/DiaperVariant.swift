public enum DiaperVariant {
	case dry
	case wet
	case wetAndDry
	case custom(String)
}


extension DiaperVariant: RawRepresentable {
	public init?(rawValue: String) {
		switch rawValue {
			case "dry":
				self = .dry
			case "wet":
				self = .wet
			case "wetAndDry":
				self = .wetAndDry
			default:
				self = .custom(rawValue)
		}
	}
	
	public var rawValue: String {
		switch self {
			case .dry:
				return "dry"
			case .wet:
				return "wet"
			case .wetAndDry:
				return "wetAndDry"
			case .custom(let value):
				return value
		}
	}
}

extension DiaperVariant: Subcategory {
	var subcategory: String? {
		switch self {
			case .dry, .wet, .wetAndDry:
				return rawValue
			case .custom:
				return String.Constants.custom
		}
	}
	
	var customLabel: String? {
		guard case .custom(let value) = self else {
			return nil
		}
		return value
	}
}

// MARK: - Hashable & Equatable
extension DiaperVariant: Hashable {}
extension DiaperVariant: Equatable {}

// MARK: - Codable
extension DiaperVariant: Decodable {}
extension DiaperVariant: Encodable {}

