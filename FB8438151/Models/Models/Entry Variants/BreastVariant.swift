public enum BreastVariant: String {
	case left
	case right
}

// MARK: - Hashable & Equatable
extension BreastVariant: Hashable {}
extension BreastVariant: Equatable {}
