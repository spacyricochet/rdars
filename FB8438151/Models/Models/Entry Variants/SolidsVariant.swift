public enum SolidsVariant {
	case custom(String)
}

extension SolidsVariant: RawRepresentable {
	public init?(rawValue: String) {
		switch rawValue {
			default:
				self = .custom(rawValue)
		}
	}
	
	public var rawValue: String {
		switch self {
			case .custom(let value):
				return value
		}
	}
}

extension SolidsVariant: Subcategory {
	var subcategory: String? {
		switch self {
			case .custom:
				return String.Constants.custom
		}
	}
	
	var customLabel: String? {
		guard case .custom(let value) = self else {
			return nil
		}
		return value
	}
}

// MARK: - Hashable & Equatable
extension SolidsVariant: Hashable {}
extension SolidsVariant: Equatable {}

// MARK: - Codable
extension SolidsVariant: Decodable {}
extension SolidsVariant: Encodable {}
