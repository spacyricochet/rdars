public enum SleepVariant: String {
	case inBed
	case asleep
}

extension SleepVariant: Subcategory {
	var subcategory: String? {
		return rawValue
	}
	
	var customLabel: String? {
		return nil
	}
}

// MARK: - Hashable & Equatable
extension SleepVariant: Hashable {}
extension SleepVariant: Equatable {}

// MARK: - Codable
extension SleepVariant: Encodable {}
extension SleepVariant: Decodable {}
