public struct Amount {
	public let value: Double
	public let unit: String
	
	public init(value: Double, unit: String) {
		self.value = value
		self.unit = unit
	}
}

// MARK: - Hashable & Equatable
extension Amount: Hashable {}
extension Amount: Equatable {}

// MARK: - Codable
