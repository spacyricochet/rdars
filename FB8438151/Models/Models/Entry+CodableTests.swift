import XCTest
@testable import Models

class EntryCodableTests: XCTestCase {
	
	func test_breastLeft_encodesAndDecodes() throws {
		let sut = Entry(id: UUID(), variant: .breast(.left), startDate: Date(), endDate: Date(), amount: Amount(value: 200, unit: "ml"))
		try sut.testEncodingAndDecoding()
	}
	
	func test_breastRight_encodesAndDecodes() throws {
		let sut = Entry(id: UUID(), variant: .breast(.right), startDate: Date(), endDate: Date(), amount: Amount(value: 200, unit: "ml"))
		try sut.testEncodingAndDecoding()
	}
	
	func test_bottleFormula_encodesAndDecodes() throws {
		let sut = Entry(id: UUID(), variant: .bottle(.formula), startDate: Date(), endDate: Date(), amount: Amount(value: 200, unit: "ml"))
		try sut.testEncodingAndDecoding()
	}
	
	func test_bottleBreastMilk_encodesAndDecodes() throws {
		let sut = Entry(id: UUID(), variant: .bottle(.breastMilk), startDate: Date(), endDate: Date(), amount: Amount(value: 200, unit: "ml"))
		try sut.testEncodingAndDecoding()
	}
	
	func test_bottleCustom_encodesAndDecodes() throws {
		let sut = Entry(id: UUID(), variant: .bottle(.custom("apple juice")), startDate: Date(), endDate: Date(), amount: Amount(value: 200, unit: "ml"))
		try sut.testEncodingAndDecoding()
	}
	
	func test_solidsCustom_encodesAndDecodes() throws {
		let sut = Entry(id: UUID(), variant: .solids(.custom("mulched banana")), startDate: Date(), endDate: Date(), amount: Amount(value: 200, unit: "ml"))
		try sut.testEncodingAndDecoding()
	}
	
	func test_diaperDry_encodesAndDecodes() throws {
		let sut = Entry(id: UUID(), variant: .diaper(.dry), startDate: Date(), endDate: Date(), amount: Amount(value: 200, unit: "ml"))
		try sut.testEncodingAndDecoding()
	}
	
	func test_diaperWet_encodesAndDecodes() throws {
		let sut = Entry(id: UUID(), variant: .diaper(.wet), startDate: Date(), endDate: Date(), amount: Amount(value: 200, unit: "ml"))
		try sut.testEncodingAndDecoding()
	}
	
	func test_diaperWetAndDry_encodesAndDecodes() throws {
		let sut = Entry(id: UUID(), variant: .diaper(.wetAndDry), startDate: Date(), endDate: Date(), amount: Amount(value: 200, unit: "ml"))
		try sut.testEncodingAndDecoding()
	}
	
	func test_diaperCustom_encodesAndDecodes() throws {
		let sut = Entry(id: UUID(), variant: .diaper(.custom("green stuff")), startDate: Date(), endDate: Date(), amount: Amount(value: 200, unit: "ml"))
		try sut.testEncodingAndDecoding()
	}
	
	func test_sleepInBed_encodesAndDecodes() throws {
		let sut = Entry(id: UUID(), variant: .sleep(.inBed), startDate: Date(), endDate: Date(), amount: Amount(value: 200, unit: "ml"))
		try sut.testEncodingAndDecoding()
	}
	
	func test_sleepAsleep_encodesAndDecodes() throws {
		let sut = Entry(id: UUID(), variant: .sleep(.asleep), startDate: Date(), endDate: Date(), amount: Amount(value: 200, unit: "ml"))
		try sut.testEncodingAndDecoding()
	}
	
	func test_medicinePill_encodesAndDecodes() throws {
		let sut = Entry(id: UUID(), variant: .medicine(.pill), startDate: Date(), endDate: Date(), amount: Amount(value: 200, unit: "ml"))
		try sut.testEncodingAndDecoding()
	}
	
	func test_medicineShot_encodesAndDecodes() throws {
		let sut = Entry(id: UUID(), variant: .medicine(.shot), startDate: Date(), endDate: Date(), amount: Amount(value: 200, unit: "ml"))
		try sut.testEncodingAndDecoding()
	}
	
	func test_medicineVitamines_encodesAndDecodes() throws {
		let sut = Entry(id: UUID(), variant: .medicine(.vitamines), startDate: Date(), endDate: Date(), amount: Amount(value: 200, unit: "ml"))
		try sut.testEncodingAndDecoding()
	}
	
	func test_medicineCustom_encodesAndDecodes() throws {
		let sut = Entry(id: UUID(), variant: .medicine(.custom("salve")), startDate: Date(), endDate: Date(), amount: Amount(value: 200, unit: "ml"))
		try sut.testEncodingAndDecoding()
	}
}

private extension Entry {
	func testEncodingAndDecoding() throws {
		let data = try JSONEncoder().encode(self)
		let decoded = try JSONDecoder().decode(Entry.self, from: data)
		XCTAssertEqual(self, decoded)
	}
}
