import Foundation

public struct Child {
	public let id: UUID?
	public var givenName: String?
    public var familyName: String?
    public var dueDate: Date?
	public var birthday: Date?
	public var gender: Gender
	
    public init(id: UUID?, givenName: String?, familyName: String?, dueDate: Date?, birthday: Date?, gender: Gender) {
		self.id = id
		self.givenName = givenName
        self.familyName = familyName
        self.dueDate = dueDate
		self.birthday = birthday
		self.gender = gender
	}
}

// MARK: - Hashable & Equatable
extension Child: Hashable {
	public func hash(into hasher: inout Hasher) {
		hasher.combine(id)
	}
}
extension Child: Equatable {}

// MARK: - Codable
extension Child: Encodable {}
extension Child: Decodable {}
