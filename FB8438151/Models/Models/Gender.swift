public enum Gender {
	case unspecified
	case male
	case female
	case other(String)
}

extension Gender: RawRepresentable {
	
	public init?(rawValue: String) {
		switch rawValue {
			case "unspecified":
				self = .unspecified
			case "male":
				self = .male
			case "female":
				self = .female
			default:
				self = .other(rawValue)
		}
	}
	
	public var rawValue: String {
		switch self {
			case .unspecified:
				return "unspecified"
			case .male:
				return "male"
			case .female:
				return "female"
			case .other(let value):
				return value
		}
	}
}

// MARK: - Hashable & Equatable
extension Gender: Hashable {}
extension Gender: Equatable {}

// MARK: - Codable
extension Gender: Encodable {}
extension Gender: Decodable {}
