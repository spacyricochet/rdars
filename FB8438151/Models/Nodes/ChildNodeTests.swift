import XCTest
import Models

class ChildNodeTests: XCTestCase {
	
	func test_initialization() {
		let entries = Set<Entry>([Entry.test()])
		let child = Child.test()
		
		let sut = ChildNode(child: child, entries: entries)
		
		XCTAssertEqual(sut.child, child)
		XCTAssertEqual(sut.entries, entries)
	}
	
	func test_addEntry_addsEntry() {
		let entry = Entry.test()
		let sut = ChildNode.test()
		
		sut.add(Set([entry]))
		
		XCTAssertTrue(sut.entries.contains(entry))
		XCTAssertEqual(sut.entries.count, 1)
	}
	
	func test_updateEntry_withNewValues_updatesValues() {
		var entry = Entry.test()
		let sut = ChildNode.test(entries: Set([entry]))
		
		let newEndDate = Date(timeInterval: 10, since: entry.endDate)
		entry.endDate = newEndDate
		sut.update(Set([entry]))
		
		XCTAssertEqual(sut.entries, Set([entry]))
	}
	
	func test_removeEntry_removesEntry() {
		let entry = Entry.test()
		let sut = ChildNode.test(entries: Set([entry]))
		
		sut.remove(Set([entry]))
		
		XCTAssertTrue(sut.entries.isEmpty)
	}
}
