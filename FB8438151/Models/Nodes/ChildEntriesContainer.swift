/// A child container connects a child with their entries and allows to update its contents.
public protocol ChildEntriesContainer {
	/// The child the entries belong to.
	var child: Child { get }
	/// All entries of the child.
	var entries: Set<Entry> { get }
	
	/// Replace the child instance with a new one, that contains updated properties.
	func update(child: Child) throws
	/// Adds the given entries.
	func add(_ entries: Set<Entry>) throws
	/// Update the given entries.
	func update(_ entries: Set<Entry>) throws
	/// Removes the given entries.
	func remove(_ entries: Set<Entry>) throws
}
