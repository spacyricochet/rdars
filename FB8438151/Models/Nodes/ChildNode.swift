public class ChildNode {
	private(set) public var child: Child
	private(set) public var entries: Set<Entry>
	
	required public init(child: Child, entries: Set<Entry>) {
		self.child = child
		self.entries = entries
	}
}

// MARK: - ChildEntriesContainer
extension ChildNode: ChildEntriesContainer {
	
	public func update(child: Child) {
		precondition(self.child.id == child.id, "The updated Child must have the same identifier.")
		self.child = child
	}
	
	public func add(_ entries: Set<Entry>) {
		self.entries.formUnion(entries)
	}
	
	public func update(_ entries: Set<Entry>) {
		let entryIds = entries.map({ $0.id })
		let oldEntries = self.entries.filter({ entryIds.contains($0.id) })
		self.entries = self.entries.subtracting(oldEntries).union(entries)
	}
	
	public func remove(_ entries: Set<Entry>) {
		self.entries.formSymmetricDifference(entries)
	}
}

/// MARK: - Convenience
extension ChildNode {
	
	public func add(entry: Entry) {
		add(Set([entry]))
	}
	
	public func update(entry: Entry) {
		update(Set([entry]))
	}
	
	public func remove(entry: Entry) {
		remove(Set([entry]))
	}
	
	public func remove(ids: Set<Entry.ID>) {
		let oldEntries = self.entries.filter({ ids.contains($0.id) })
		self.entries.formSymmetricDifference(oldEntries)
	}
}
