import Foundation
import Models

extension Entry {
	/// Returns a new instance with a new identifier.
	static func test() -> Self {
		.init(id: UUID(),
					variant: .weight,
					startDate: Date(timeIntervalSince1970: 0),
					endDate: Date(timeIntervalSince1970: 0),
					amount: Amount(value: 1000, unit: "g"))
	}
}
