import Models

extension ChildNode {
	/// Returns an instance with new instance of Child and the given entries.
	static func test(child: Child = .test(), entries: Set<Entry> = .init()) -> Self {
		.init(child: .test(), entries: entries)
	}
}
