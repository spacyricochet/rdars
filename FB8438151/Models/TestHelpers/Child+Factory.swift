import Foundation
import Models

extension Child {
	/// Returns a new, empty instance.
	static func test() -> Self {
        Child.init(id: nil,
                   givenName: nil,
                   familyName: nil,
                   dueDate: nil,
                   birthday: nil,
                   gender: .unspecified)
	}
}
