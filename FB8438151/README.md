# Title

NSPersistentContainer(name: "ModelName") does not load managed object model.

# Basic information

| Key | Value |
|---|---|
| # | FB8438151 |
| Status | Closed |
| Area | Core Data API |
| Type of issue | Incorrect/Unexpected Behavior |
| Date | 2020-08-17 |
| Xcode | 12.0 beta 4 (12A8179i) |

# Description

After creating a data model in a framework and creating a convenience class for maintaining the persistent store and writing a test for the framework, the tests fail due to the use of Core Data’s convenience initializer for NSPersistentContainer;

```
public convenience init(name: String)
```

It fails to load the managed object model as expected.

## Steps

1. Open attached project.
2. Select target ‘Storage’.
3. Run tests for ‘Storage’ (i.e. the target ‘StorageTests’).


(See attached video ‘Reproduction’)

## Expected

The tests succeed.

## Actual

The tests fail with the following log;

```
Warning: Error creating LLDB target at path '/Applications/Xcode-12.0-beta4.app/Contents/Developer/Platforms/iPhoneSimulator.platform/Developer/Library/Xcode/Agents/xctest'- using an empty LLDB target which can cause slow memory reads from remote devices.
Test Suite 'All tests' started at 2020-08-17 17:41:17.126
Test Suite 'StorageTests.xctest' started at 2020-08-17 17:41:17.127
Test Suite 'Storage_ObjectsTests' started at 2020-08-17 17:41:17.127
Test Case '-[StorageTests.Storage_ObjectsTests test_createLog_createsAnEmptyLog]' started.
2020-08-17 17:41:17.129446+0200 xctest[54009:8852104] [error] error:  Failed to load model named DataStorage
CoreData: error:  Failed to load model named DataStorage
2020-08-17 17:41:17.140273+0200 xctest[54009:8852104] [error] error: No NSEntityDescriptions in any model claim the NSManagedObject subclass 'Storage.ManagedLog' so +entity is confused.  Have you loaded your NSManagedObjectModel yet ?
CoreData: error: No NSEntityDescriptions in any model claim the NSManagedObject subclass 'Storage.ManagedLog' so +entity is confused.  Have you loaded your NSManagedObjectModel yet ?
2020-08-17 17:41:17.140457+0200 xctest[54009:8852104] [error] error: +[Storage.ManagedLog entity] Failed to find a unique match for an NSEntityDescription to a managed object subclass
CoreData: error: +[Storage.ManagedLog entity] Failed to find a unique match for an NSEntityDescription to a managed object subclass
/Users/brunoscheele/Projects/feedme/Storage/DataStorage+ObjectsTests.swift:20: error: -[StorageTests.Storage_ObjectsTests test_createLog_createsAnEmptyLog] : An NSManagedObject of class 'Storage.ManagedLog' must have a valid NSEntityDescription. (NSInvalidArgumentException)
Test Case '-[StorageTests.Storage_ObjectsTests test_createLog_createsAnEmptyLog]' failed (0.037 seconds).
Test Suite 'Storage_ObjectsTests' failed at 2020-08-17 17:41:17.164.
	 Executed 1 test, with 1 failure (1 unexpected) in 0.037 (0.037) seconds
Test Suite 'StorageTests.xctest' failed at 2020-08-17 17:41:17.165.
	 Executed 1 test, with 1 failure (1 unexpected) in 0.037 (0.038) seconds
Test Suite 'All tests' failed at 2020-08-17 17:41:17.165.
	 Executed 1 test, with 1 failure (1 unexpected) in 0.037 (0.039) seconds
Program ended with exit code: 1
```

## Workaround

Use `public init(name: String, managedObjectModel model: NSManagedObjectModel)` instead.

# Discussion

## Apple feedback (2020-08-19)

An alternative approach would be to have a framework subclass NSPersistentContainer as menteiond in the WWDC Talk from 2018, see this link for more detailed info:
https://developer.apple.com/videos/play/wwdc2018/224/

# Resolution

Feedback received. Since outdated.
