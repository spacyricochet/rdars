#  Credits

## Learning resources

- [Using Core Data with SwiftUI 2.0 and Xcode 12 – Donny Wals](https://www.donnywals.com/using-core-data-with-swiftui-2-0-and-xcode-12/)
- [How to configure Core Data to work with SwiftUI – Hacking with Swift](https://www.hackingwithswift.com/quick-start/swiftui/how-to-configure-core-data-to-work-with-swiftui)

