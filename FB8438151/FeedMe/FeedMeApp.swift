import SwiftUI

@main
struct FeedMeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
