import SwiftUI

struct SingleSelectionTogglesView: View {
	
	@Namespace var namespace
	@State var buttons = [ButtonViewModel]()
	@State var selectedButton: ButtonViewModel?
	
	var body: some View {
		HStack {
			ForEach(buttons) { button in
				ToggleButton(
					isSelected: .constant(selectedButton?.id == button.id),
					viewModel: button,
					namespaceId: namespace,
					handler: { item in
						if selectedButton?.id != item.id {
							selectedButton = item
						} else {
							selectedButton = nil
						}
					})
			}
		}
	}
}

#if DEBUG
struct ColoredRadioButtonsView_Previews: PreviewProvider {
	static var previews: some View {
		let view = SingleSelectionTogglesView(buttons: buttons, selectedButton: buttons[0])
		return view
	}
}

extension ColoredRadioButtonsView_Previews {
	static let buttons = [
		ButtonViewModel(id: "test1",
					  icon: UIImage.init(systemName: "star")!,
					  title: "Test 1",
					  accessibilityLabel: "Test label",
					  accessibilityHint: "test hint",
					  normalTintColor: .systemRed,
					  selectedTintColor: .darkText),
		ButtonViewModel(id: "test2",
					  icon: UIImage.init(systemName: "star")!,
					  title: "Test 2",
					  accessibilityLabel: "Test label",
					  accessibilityHint: "test hint",
					  normalTintColor: .systemBlue,
					  selectedTintColor: .darkText),
		ButtonViewModel(id: "test3",
					  icon: UIImage.init(systemName: "star")!,
					  title: "Test 3",
					  accessibilityLabel: "Test label",
					  accessibilityHint: "test hint",
					  normalTintColor: .systemGreen,
					  selectedTintColor: .darkText),
	]
}
#endif

extension View {
	func addBackground(color: UIColor, namespace: Namespace.ID, condition: Bool) -> some View {
		@ViewBuilder
		var result: some View {
			if condition {
				background(
					RoundedRectangle(cornerRadius: 25.0, style: .continuous)
						.fill(Color(color))
						.matchedGeometryEffect(id: "indicator", in: namespace)
				)
			} else {
				self
			}
		}
		return result
	}
}
