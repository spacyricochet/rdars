import UIKit

struct ButtonViewModel: Identifiable {
	var id: String
	var icon: UIImage
	var title: String
	var accessibilityLabel: String?
	var accessibilityHint: String?
	var normalTintColor: UIColor
	var selectedTintColor: UIColor
}
