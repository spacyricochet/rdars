# Title

iCloud+ Feature Updates scrollable area does not extend to unfolded 'new' items

# Basic information

| Key | Value |
|---|---|
| # | FB9645509 |
| Status | Closed |
| Area | iOS & iPadOS | Settings > iCloud |
| Type of issue | Incorrect/Unexpected Behavior |
| OS | iOS 15.0 |

# Steps

1. Update to iOS 15.0.
2. Go to Settings app.
3. Tap ‘iCloud+ Feature Updates’ below iCloud user information.
4. Unfold more than one item.
5. Scroll to read everything.

## Expected

Being able to scroll to read everything.

## Actual

The scrollable area does not enlarge when unfolding items, causing them to be cut off.

# Resolution

Outdated and feature not available anymore.
