# Title 

Add option to extend the List's row separator across the entire visual row

# Basic information

| Key | Value |
|---|---|
| # | FB16438840 |
| Status | Open |
| Platform | iOS |
| Technology | SwiftUI |
| Type of issue | Suggestion |
| Date | 2025-02-02 |
| Xcode | Version 16.0 (16A242d) |
| OS | iOS 18.1.1 (22B91) |

# Description

We would like to have a proper option to extend the List’s row separator across the entire visual cell, instead of just to the edge of the content. 

Currently, in the default state a List’s row has a separator that goes from the content’s first text view in the row to the very trailing edge of the cell. An incredibly common design is to extend this separator across the entire cell, from the very leading edge to the very trailing edge of the cell. However, there does not appear to be a way to set the leading point of the separator all the way to the edge, like its trailing point.

This same effect can be achieved through workarounds, but all of these have the disadvantage that they break and limit a lot of iOS’s standard List affordances, which then have to be replicated—often incompletely. It would be a great help to have an official API that can set the separator to span the entire cell’s width.

# Files

* [Desired Design](DesiredDesign.png) | The desired design, where the list row separator spans the entire cell.
* [ListRowSeparators](./ChartsFKSSupport/) | A project showing some workarounds, all inadequate.
