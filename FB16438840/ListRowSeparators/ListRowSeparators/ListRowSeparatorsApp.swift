import SwiftUI

@main
struct ListRowSeparatorsApp: App {
	var body: some Scene {
		WindowGroup {
			ContentView()
		}
	}
}
