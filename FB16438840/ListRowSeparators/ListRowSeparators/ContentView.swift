import SwiftUI

struct ContentView: View {
	var body: some View {
		TabView {
			Tab("Default", systemImage: "list.bullet") {
				ListDefault()
			}
			Tab("Leading", systemImage: "list.dash") {
				ListWithLeadingSeparator()
			}
			Tab("Out of bounds", systemImage: "list.triangle") {
				ListWithBeyondEdgeSeparator()
			}
			Tab("Custom", systemImage: "list.star") {
				ListWithCustomSeparator()
			}
		}
	}
}

struct ListContent: View {
	var body: some View {
		ForEach(0..<5) {
			Label("Item \($0)", systemImage: "star")
		}
	}
}

struct ListDefault: View {
	var body: some View {
		List {
			Text("The default setup. Nice, but we want the separator's leading point to extend to the edge of the cell, like the trailing point.")
			Section {
				ListContent()
			}
		}
	}
}

struct ListWithLeadingSeparator: View {
	var body: some View {
		List {
			Text("The separators' leading point here do not extend past the row content.")
			Section {
				ListContent()
					.alignmentGuide(.listRowSeparatorLeading) { dim in
						dim[.leading]
					}
			}
		}
	}
}

struct ListWithBeyondEdgeSeparator: View {
	var body: some View {
		List {
			Text("The separators here extends way past the edge. Because we have no idea what the padding between the row content and the edge is, we cannot set this reliably.")
			Section {
				ListContent()
					.alignmentGuide(.listRowSeparatorLeading) { _ in
						-100
					}
			}
		}
	}
}

struct ListWithCustomSeparator: View {
	var body: some View {
		List {
			Text("Adding an overlay to the background kind of works, but we now also have to keep track when a separator is necessary.")
			Section {
				ListContent()
					.listRowBackground(
						Color.white
							.overlay(alignment: .bottom) {
								Color.black
								.frame(width: 2000, height: 1)
								.offset(x: -100)
							}
					)
			}
		}
		.listRowSeparator(.hidden)
	}
}

#Preview {
	ContentView()
}
#Preview {
	ListWithLeadingSeparator()
}
#Preview {
	ListWithBeyondEdgeSeparator()
}
#Preview {
	ListWithCustomSeparator()
}
