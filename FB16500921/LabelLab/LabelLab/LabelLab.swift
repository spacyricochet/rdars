import SwiftUI

struct LabelLab: View {
	@State private var showList = true
	
	@Environment(\.dynamicTypeSize) var dynamicTypeSize
	
	var body: some View {
		Text("Views below the label's icon should not have a first line indent on accessibility text sizes")
			.padding()
			.font(.headline)
			.accessibilityAddTraits(.isHeader)
		Toggle("Use list", isOn: $showList)
			.padding(.horizontal)
		if showList {
			List {
				content
			}
		} else {
			ScrollView {
				VStack(alignment: .leading) {
					content
				}
				.padding()
			}
		}
	}
	
	@ViewBuilder
	private var content: some View {
		Text("To see the issue, please increase the text size to accessibility text sizes.")
		Label(title: {
			VStack(alignment: .leading) {
				if (dynamicTypeSize.isAccessibilitySize) {
					Text("On accessibility text sizes, the icon anchors to the top of the row and the first line of the Text starts next to the circle and subsequent lines below the circle, as expected.")
					Text("Text lower than the icon should not have the first line indent though. The first line should start next to below the icon.")
				} else {
					Text("On regular text sizes, all lines of the Text start next to the icon, and the icon is centered along the entire label. This looks good at the smaller sizes and is expected.")
					Text("This also happens on other texts in the label, where the top is below starts below the icon.")
				}
			}
		}, icon: {
			Circle().fill(Color.blue).frame(width: 24, height: 24)
		})
		if dynamicTypeSize.isAccessibilitySize {
			Text("Or alternatively, have some kind of option to avoid the first line indent behaviour for next texts.")
		}
		if !showList {
			Text("The behaviour doesn't happen outside of a List.")
				.fontWeight(.medium)
		}
	}
}

#Preview {
	LabelLab()
}
