//
//  LabelLabApp.swift
//  LabelLab
//
//  Created by Bruno Scheele on 14/02/2025.
//

import SwiftUI

@main
struct LabelLabApp: App {
    var body: some Scene {
        WindowGroup {
					LabelLab()
        }
    }
}
