# Title 

Views below the label's icon should not have a first line indent on accessibility text sizes

# Basic information

| Key | Value |
|---|---|
| # | FB16500921 |
| Status | Open |
| Area | iOS |
| Technology | SwiftUI |
| Type of issue | Incorrect/Unexpected Behavior |
| Date | 2025-02-14 |
| Xcode | 16.0 (16A242d) |
| OS | iOS 18.1 |

# Description

In a list with accessibility text sizes, a `Label` with multiple Texts in the `label` property will have the first line of all the Texts start _next_ to the `icon`, but all subsequent lines start _below_ the `icon`. This behavior looks great if there’s only one Text as a Label, but pretty bad if there’s one larger title Text and multiple smaller detail and subtitle Texts.

# Steps

1. Open attached project LabelLab.
2. Run LabelLab on Simulator, Previews or iOS device.
3. Set device to accessibility text sizes.

## Expected

* The first Text’s first line starts next to the icon and the subsequent lines start below the icon.
* The second and further Texts have all lines start below the icon.

## Actual

* ✅ The first Text’s first line starts next to the icon and the subsequent lines start below the icon.
* ❌ The second and further Texts all have their first line start next to the icon, and subsequent lines below the icon — just like the first Text.

# Files

* [LabelLab.zip](./LabelLab.zip)
