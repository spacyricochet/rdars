//
//  FooManagedObject+CoreDataProperties.swift
//  CoreDataGeneratorBug
//
//  Created by Bruno Scheele on 03/08/2020.
//
//

import Foundation
import CoreData


extension FooManagedObject {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<FooManagedObject> {
        return NSFetchRequest<FooManagedObject>(entityName: "Foo")
    }


}

extension FooManagedObject : Identifiable {

}
