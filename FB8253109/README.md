# Title

NSManagedObject class generation places file references in incorrect group

# Basic information

| Key | Value |
|---|---|
| # | FB8253109 |
| Status | Closed |
| Area | Xcode |
| Type of issue | Incorrect/Unexpected Behavior |
| Date | 2020-08-03 |
| Xcode | 12.0 beta 3 (12A8169g) | 

# Description

When generating NSManagedObject subclasses for a Core Data model, the expectation is that references in the project folder they are stored in the group that corresponds to the selected folder when selecting the place for the files.

Example;

When generating files for the model Foo for target Bar, I do the following;
* I set the target to include the files in to ‘Foo’.
* I select the folder ‘Foo’ to put the files in. This folder corresponds to the group ‘Foo’ in my project file.

I expect the files to be placed in the selected folder and the file references to be placed group ‘Foo’.

Actually, the files are in the correct folder, but the references are in the top level of the project file instead, requiring the user to move them manually.

# Steps

1. Open attached project.
2. Select ‘Model.xcdatamodeld’
3. Select menu item ‘Editor > Create NSManagedObject Subclass…’
4. Ensure ‘Model’ is selected and press ‘Next’.
5. Ensure ‘Foo’ is selected and press ‘Next’.
6. Ensure target ‘CoreDataGeneratorBug’ is selected.
7. Select folder ‘CoreDataGeneratorBug’ to place files in.
8. Press ‘Create’.

(See attached video ‘Reproduction’)

## Expected

The references to the files `FooManagedObject+CoreDataObject.swift` and `FooManagedObject+CoreDataProperties.swift` are placed in the group `CoreDataGenaratorBug`.

(See attached screenshot ‘Expected’)

## Actual

The references to the files `FooManagedObject+CoreDataObject.swift` and `FooManagedObject+CoreDataProperties.swift` are placed at the top level of the project.

(See attached screenshot ‘Actual’)

# Resolution

Outdated.
