# Title

Regenerating NSManagedObject subclass after moving file references in project creates duplicate references

# Basic information

| Key | Value |
|---|---|
| # | FB8253224 |
| Status | Closed |
| Area | Xcode |
| Type of issue | Application Crash |
| Date | 2020-08-03 |
| Xcode | 12.0 beta 3 (12A8169g) |

# Description

When generating NSManagedObject subclasses, Xcode places the file references in the top level of the project, regardless of the folder selected for the files and how they correspond with groups in the project (related bug; FB8253109).

If you move these references to the correct/expected group, and later regenerate the NSManagedObject subclass, it created a duplicate reference for the newly generated/replaced file.

## Steps

1. Open attached project.
2. Select ‘Model.xcdatamodeld’.
3. Select ‘Editor > Create NSManagedObject Subclass…’
4. Ensure ‘Model’ is selected and tap ‘Next’.
5. Ensure ‘Foo’ is selected and tap ‘Next’.
6. Select ‘CoreDataGeneratorBug’ and tap ‘Create’.
7. Move new file references of `FooManagedObject+CoreDataObject.swift` and `FooManagedObject+CoreDataProperties.swift` to the group ‘CoreDataGeneratorBug’.
8. Repeat steps 2–6.

(See attached video ‘Reproduction’)

## Expected

No new references being created.

(See attached screenshot ‘Expected’)

## Actual

A duplicate reference to ‘FooManagedObject+CoreDataProperties.swift’ is created at the top level of the project.

(See attached screenshot ‘Actual’)

# Resolution

Outdated.
