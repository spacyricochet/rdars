# Title

Linelimit >1 in Text View not respected

# Basic information

| Key | Value |
|---|---|
| # | FB6342015 |
| Status | Closed |
| Area | feedbackassistant.apple.com |
| Type of issue | Type of issue |
| Date | 28/06/2019 |

# Description

macOS Feedback Assistant Version 4.6 (400)

When uploading files in a new issue, when reviewing that issue, the file is mistakingly called 'iOS Sysdiagnose' instead of whatever the file is actually called.

# Files

* ![Uploaded files](1_uploaded_files.png)
* ![Problem review](2_problem_review.png)

# Resolution

Unknown. Outdated. Access to original ticket lost.
