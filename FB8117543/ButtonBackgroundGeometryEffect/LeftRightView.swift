import SwiftUI

struct LeftRightView: View {
    @State var left = true
    @Namespace var namespace
    
    var body: some View {
        VStack {
            Button("Toggle", action: { withAnimation { left.toggle() } })
            if left { LeftView(namespace: namespace) }
            else { RightView(namespace: namespace) }
        }
    }
}

struct LeftView: View {
    var namespace: Namespace.ID
    
    var body: some View {
        HStack {
            Color.red.frame(width: 100, height: 100)
                .matchedGeometryEffect(id: "box", in: namespace)
            Spacer()
        }
        .padding()
    }
}

struct RightView: View {
    var namespace: Namespace.ID
    
    var body: some View {
        HStack {
            Spacer()
            Color.blue.frame(width: 100, height: 100)
                .matchedGeometryEffect(id: "box", in: namespace)
        }
        .padding()
    }
}

#if DEBUG
struct LeftRightView_Previews: PreviewProvider {
    static var previews: some View {
        LeftRightView()
    }
}
#endif
