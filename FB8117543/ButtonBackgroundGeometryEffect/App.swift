//
//  ButtonBackgroundGeometryEffectApp.swift
//  ButtonBackgroundGeometryEffect
//
//  Created by Bruno Scheele on 23/07/2020.
//

import SwiftUI

@main
struct ButtonBackgroundGeometryEffectApp: App {
    var body: some Scene {
        WindowGroup {
            Text("Hello world!")
        }
    }
}
