import SwiftUI

struct SingleSelectionTogglesView: View {
    
    @Namespace var namespace
    @State var buttons = [ButtonViewModel]()
    @State var selectedButton: ButtonViewModel?
    
    var body: some View {
        HStack {
            ForEach(buttons) { button in
                ToggleButton(
                    isSelected: .constant(selectedButton?.id == button.id),
                    viewModel: button,
                    namespaceId: namespace,
                    handler: { item in
                        if selectedButton?.id != item.id {
                            selectedButton = item
                        } else {
                            selectedButton = nil
                        }
                    })
            }
        }
    }
}

#if DEBUG
struct ColoredRadioButtonsView_Previews: PreviewProvider {
    static var previews: some View {
        let view = SingleSelectionTogglesView(buttons: ButtonViewModel.buttons, selectedButton: ButtonViewModel.buttons[0])
        return view
    }
}
#endif

extension View {
    func addBackground(color: UIColor, namespace: Namespace.ID, condition: Bool) -> some View {
        @ViewBuilder
        var result: some View {
            if condition {
                background(
                    RoundedRectangle(cornerRadius: 25.0, style: .continuous)
                        .fill(Color(color))
                        .matchedGeometryEffect(id: "indicator", in: namespace)
                )
            } else {
                self
            }
        }
        return result
    }
}

struct ToggleButton: View {
    
    @Binding var isSelected: Bool
    let viewModel: ButtonViewModel
    var namespaceId: Namespace.ID
    private let handler: ((ButtonViewModel) -> Void)?
    
    init(isSelected: Binding<Bool>, viewModel: ButtonViewModel, namespaceId: Namespace.ID, handler: ((ButtonViewModel) -> Void)?) {
        self._isSelected = isSelected
        self.viewModel = viewModel
        self.namespaceId = namespaceId
        self.handler = handler
    }
    
    var body: some View {
        var result = Button(action: {
            isSelected.toggle()
            handler?(viewModel)
        },
        label: {
            let accentColor = isSelected ? viewModel.selectedTintColor : viewModel.normalTintColor
            let backgroundColor = isSelected ? viewModel.normalTintColor : viewModel.selectedTintColor
            VStack {
                Image(uiImage: viewModel.icon)
                Text(viewModel.title)
            }
            .padding()
            .accentColor(Color(accentColor))
            .addBackground(color: backgroundColor,
                           namespace: namespaceId,
                           condition: isSelected)
        })
        .accessibility(identifier: viewModel.id)
        .accessibility(label: Text(viewModel.accessibilityLabel ?? viewModel.title))

        if let hint = viewModel.accessibilityHint {
            result = result.accessibility(hint: Text(hint))
        }
        
        return result
    }
}

struct ButtonViewModel: Identifiable {
    var id: String
    var icon: UIImage
    var title: String
    var accessibilityLabel: String?
    var accessibilityHint: String?
    var normalTintColor: UIColor
    var selectedTintColor: UIColor
}

extension ButtonViewModel {
    static let buttons = [
        ButtonViewModel(id: "test1",
                      icon: UIImage.init(systemName: "star")!,
                      title: "Test 1",
                      accessibilityLabel: "Test label",
                      accessibilityHint: "test hint",
                      normalTintColor: .systemRed,
                      selectedTintColor: .darkText),
        ButtonViewModel(id: "test2",
                      icon: UIImage.init(systemName: "star")!,
                      title: "Test 2",
                      accessibilityLabel: "Test label",
                      accessibilityHint: "test hint",
                      normalTintColor: .systemBlue,
                      selectedTintColor: .darkText),
        ButtonViewModel(id: "test3",
                      icon: UIImage.init(systemName: "star")!,
                      title: "Test 3",
                      accessibilityLabel: "Test label",
                      accessibilityHint: "test hint",
                      normalTintColor: .systemGreen,
                      selectedTintColor: .darkText),
    ]
}
