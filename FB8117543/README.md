# Title

Expected animation with matchedGeometryEffect of Views in Button background not happening

# Basic information

| Key | Value |
|---|---|
| # | FB8117543 |
| Status | Closed |
| Area | SwiftUI Framework |
| Type of issue | Incorrect/Unexpected Behavior |
| Date | 2020-07-23 |
| Xcode | 12.0 beta 3 (12A8169g) |

# Description

Attempting to use `matchedGeometryEffect` to create an animation between views in the background of Buttons fails to animate them. 

The problem arises in SingleSelectionTogglesViews.swift, where depending on the selected button, the background is filled with a RoundedRectangle. Through the use of `matchedGeometryEffect`, this RoundedRectangle should animate between buttons, but it fails to do this.

This follows the same principle as seen in LeftRightView, where the `namespace` is passed through to other custom Views through a `Namespace.ID` property.

## Steps

1. Open attached project.
2. View SingleSelectionTogglesView.swift.
3. Start Canvas Preview.
4. Tap the differently colored buttons.

## Expected

The background view animated from button to button.

## Actual

The background view fades out from the previous button and fades in with the currently selected button.

# Resolution

Outdated.
